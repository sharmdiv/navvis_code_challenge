package com.qa.web.pages;

import com.qa.web.base.TestBase;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.qa.web.utils.TestUtil.clickOn;

import java.util.concurrent.TimeUnit;

public class MarkandMeasurePageActions extends TestBase {

	@FindBy(xpath = "(//I[@icon-class='material-icons'][text()='straighten'])[1]")
	public WebElement measure;
	//*[@id="tooltip-8"]/icon/i

	@FindBy(xpath = "//button[normalize-space()='Accept']")
	public WebElement cookieAccept;

	@FindBy(xpath = "//img[@title='Free Distance']")
	private WebElement freeDistance;

	@FindBy(xpath = "//img[@title='Horizontal Distance']")
	private WebElement horizontalDistance;

	@FindBy(xpath = "//img[@title='Vertical Distance']")
	private WebElement verticalDistance;

	@FindBy(xpath = "//li[contains(text(),'Item 3')]")
	private WebElement Item3;

	WebDriverWait wait = new WebDriverWait(driver, 20);

	public MarkandMeasurePageActions() {
		PageFactory.initElements(driver, this);
	}

	public void selectDroppableInteraction() {
		clickOn(driver, measure);

	}

	public void cookieAccept() {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		//cookieAccept.click();
		driver.manage().timeouts().implicitlyWait(250, TimeUnit.SECONDS);

	}

	public void measureAssert() throws InterruptedException {
		Thread.sleep(250);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//I[@icon-class='material-icons'][text()='straighten'])[1]")));
	
	}

	public void measureClick() throws InterruptedException {
		Thread.sleep(200);
		//driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Actions act =  new Actions(driver);
		act.moveToElement(measure).build().perform();
		driver.manage().timeouts().implicitlyWait(250, TimeUnit.SECONDS);
		act.moveToElement(measure).click().perform();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

	}

	public void markAndMeasureAssert() {
		Assert.assertEquals(true, freeDistance.isDisplayed());
		Assert.assertEquals(true, horizontalDistance.isDisplayed());
		Assert.assertEquals(true, verticalDistance.isDisplayed());

	}

}
