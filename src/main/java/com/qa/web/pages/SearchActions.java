package com.qa.web.pages;

import com.qa.web.base.TestBase;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.qa.web.utils.TestUtil.clickOn;

import java.util.concurrent.TimeUnit;

public class SearchActions extends TestBase {

	@FindBy(xpath = "//input[@placeholder='Search in M�nchen - Maxvorstadt']")
	public WebElement search;

	@FindBy(xpath = "//button[normalize-space()='Accept']")
	public WebElement cookieAccept;

	WebDriverWait wait = new WebDriverWait(driver, 20);

	public SearchActions() {
		PageFactory.initElements(driver, this);
	}

	public void searchAssert() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Assert.assertEquals(true, search.isDisplayed());
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	public void searchText() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Actions act =  new Actions(driver);
		act.moveToElement(search).build().perform();
		driver.manage().timeouts().implicitlyWait(250, TimeUnit.SECONDS);
		act.moveToElement(search).click().perform();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		search.sendKeys("Floor 4");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		search.sendKeys(Keys.ENTER);
	}

	public void cookieAccepts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//cookieAccept.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

}
