package com.qa.web.test;

import com.qa.web.base.TestBase;
import com.qa.web.pages.MarkandMeasurePageActions;
import com.qa.web.pages.SearchActions;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SearchFloorTest extends TestBase {

	SearchActions searchPageActions;

	public SearchFloorTest() {
		super();

	}

	@org.testng.annotations.BeforeClass
	public void setup() {
		Initialization();
		searchPageActions = new SearchActions();

	}

	@Test
	public void searchAssert() {

		searchPageActions.cookieAccepts();
		searchPageActions.searchAssert();
		searchPageActions.searchText();

	}

	@Test
	public void searchingText() {
		searchPageActions.cookieAccepts();
		searchPageActions.searchText();

	}

	@AfterClass
	public void teardown() {
		driver.close();
		driver.quit();
	}

}
