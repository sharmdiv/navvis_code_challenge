package com.qa.web.test;

import com.qa.web.base.TestBase;
import com.qa.web.pages.MarkandMeasurePageActions;

import org.junit.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MarkAndMeasure extends TestBase {

	MarkandMeasurePageActions pageActions;

	public MarkAndMeasure() {
		super();

	}

	@BeforeClass
	public void setup() {
		Initialization();
		pageActions = new MarkandMeasurePageActions();

	}

	@Test
	public void assertingMeasure() throws InterruptedException {
		pageActions.cookieAccept();
		pageActions.measureAssert();
		

	}
	@Test
	public void measureTab() throws InterruptedException {
		pageActions.measureClick();
		pageActions.markAndMeasureAssert();
		
	}

	@AfterClass
	public void teardown() {
		driver.close();
		driver.quit();
	}

}
